# CHAT

## Live Demo
https://shrouded-thicket-11517.herokuapp.com/

## How to use
Make sure you have Node.js and the npm  installed
```
$ git clone https://dmitry_1@bitbucket.org/dmitry_1/chat.git 
$ cd chat
$ npm install
$ npm start
```
And point your browser to http://localhost:3000

## Features
- Multiple users can join a chat room and each users input username on website load.
- When a user connected a notification shows the user how many users are online.
- Users can type chat messages to the chat room.
- when the user types a message, the notification shows other users that the user is typing.
- A notification is sent to all users when a user joins or leaves the chatroom.
- Also users can see the time of the message was sent.
